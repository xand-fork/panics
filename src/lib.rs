#![forbid(unsafe_code)]

#[macro_use]
extern crate tpfs_logger_port;

use std::panic::{set_hook, take_hook, PanicInfo};

#[logging_event]
enum LoggingEvent {
    Panic(String),
}

fn new_logging_hook() -> Box<dyn Fn(&PanicInfo) + Send + Sync> {
    let original_hook = take_hook();
    let new_hook = move |info: &PanicInfo| {
        let location = info.location().unwrap();
        let panic_msg = match info.payload().downcast_ref::<&'static str>() {
            Some(s) => *s,
            None => match info.payload().downcast_ref::<String>() {
                Some(s) => &s[..],
                None => "Box<Any>",
            },
        };
        let log_msg = format!("Panicked at '{}', {}", panic_msg, location);
        error!(LoggingEvent::Panic(log_msg));
        original_hook(info);
    };
    Box::new(new_hook)
}

/// Sets a new hook to be called when code panics. The new hook will include whichever hook is already
/// set for panics in addition to also logging the message and location of the panic.
pub fn set_log_panics_hook() {
    set_hook(new_logging_hook());
}
